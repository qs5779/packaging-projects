#!/bin/bash -ex
# -*- Mode: Bash; tab-width: 2; indent-tabs-mode: nil -*-
# Revision History:
# 20211224 - que - initial version
#

# enter the directory where the build script is located
cd "${0%/*}"
SCRIPT=$(basename "$0")
ERRORS=0
VERSION=0.8.3
BUILD_NUMBER=0
BUILD_DEBUG=0
TEST_DEBUG=0
CODENAME=all

usage() {
  cat << EOM
usage: $SCRIPT [-b build_number} [-C] [-c codename] [-T] [-t] [-v version]
  where:
    -b build_number - specify build number (default 0)
    -C              - specify clean and exit
    -c codename     - specify codename to build (default all)
    -h              - show this message and exit
    -T              - specify build debug
    -t              - specify test debug
    -v version      - specify version to build (default 0.8.3)
EOM
  exit 1
}

cleanup() {
  rm -fr ruby-install-v${VERSION}
}

while getopts ":b:Cc:hTtv:" opt
do
  case "$opt" in
    b ) BUILD_NUMBER=${BUILD_NUMBER} ;;
    C )
      cleanup
    ;;
    c ) CODENAME=${OPTARG} ;;
    h )
      usage
    ;;
    T ) BUILD_DEBUG=1 ;;
    t ) TEST_DEBUG=1 ;;
    v ) VERSION=${OPTARG} ;;
    * )
      echo "Unexpected option \"$opt\""
      usage
    ;;
  esac
done
shift $((OPTIND - 1))

SRCD="./ruby-install-${VERSION}"
if [[ ! -d "$SRCD" ]]
then
  TBAL="/tmp/ruby-install-${VERSION}.tar.gz"
  curl -C - -L -o "$TBAL" "https://github.com/postmodern/ruby-install/archive/v${VERSION}.tar.gz"
  tar -xzf "$TBAL"
  if [[ ! -d "$SRC_DIR" ]]
  then
    echo "Directory not found: $SRCD" >&2
    exit 1
  fi
fi

if [[ "$CODENAME" == "all" ]]
then
  for df in $(ls packaging/targets/build-image/Dockerfile.* 2>/dev/null)
  do
    CN=$(echo "$df" | sed 's!.*Dockerfile\.!!')
    if [[ -n "$CNAMES" ]]
    then
      CNAMES="$CNAMES $CN"
    else
      CNAMES="$CN"
    fi
  done
else
  CNAMES="$CODENAME"
fi

for cn in $CNAMES
do
  case "$cn" in
    buster )
      OSNAME=debian
    ;;
    focal|impish )
      OSNAME=ubuntu
    ;;
    * )
      echo "Unsupported codename: $cn" >&2
      exit 1
    ;;
  esac

  cat << EOF > ./packaging/env.list
VERSION=${VERSION}
DCODENM=${cn}
OSNAME=${OSNAME}
BUILD_NUMBER=${BUILD_NUMBER}
BUILD_DEBUG=${BUILD_DEBUG}
TEST_DEBUG=${TEST_DEBUG}
EOF

  packaging/targets/build
  RC=$?
  if [[ "$RC" == "0" ]]
  then
    for deb in $(ls packaging/targets/out/*.deb 2>/dev/null)
    do
      mv -fv "$deb" /var/docker/aptly/data/automation/incoming
    done
  else
    echo "packaging/targets/build $cn $VERSION exit code: $RC" >&2
    ((ERRORS+=1))
  fi
done


# ERRORS=$((ERRORS+=1)) # darn ubuntu default dash shell
exit $ERRORS

# vim:sta:et:sw=2:ts=2:syntax=sh
