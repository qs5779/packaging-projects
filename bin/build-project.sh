#!/bin/bash -e
# -*- Mode: Bash; tab-width: 2; indent-tabs-mode: nil -*-
# Revision History:
# 20211227 - que - initial version
#

# save directory where the build script is located
SCRDIR="${0%/*}"
SCRIPT=$(basename "$0")
ERRORS=0
DEBUG=0
CLEANUP=0
BUILD_NUMBER=0
BUILD_DEBUG=0
TEST_DEBUG=0
CODENAMES=''
REPONAME=main
ITERATION=0

function add_code_name {
  typeset I="$1"
  typeset D="${2:- }" #delimeter could be any char,default space

  if [ -n "$I" ]
  then
    # allow user to use space, comma or semicolon as delimeter and change to ours
    #I=$(echo $I | sed -e "s/ */${DELIMITER}/g" -e "s/[,;]\+/${DELIMITER}/g")
    I=$(perl -e "print join('${D}', split(/[,; ]+/, join(',', @ARGV)))" "$I")
    if [ -n "$ENVIRONMENTS" ]
    then
      CODENAMES="$ENVIRONMENTS $I"
    else
      CODENAMES="$I"
    fi
  fi
}

usage() {
  cat << EOM
usage: $SCRIPT [-b build_number} [-C] [-c codename] [-T] [-t] [-v version]
  where:
    -b build_number - specify build number (default 0)
    -C              - specify clean and exit
    -c codename     - specify codename to build (default all)
    -d              - specify debug $SCRIPT
    -h              - show this message and exit
    -i iteration    - specify build iteration which appends to version (default 0) ex: -i 1
    -r reponame     - specify repo name to target (default main) ex: -r testing
    -T              - specify build debug
    -t              - specify test
    -v version      - specify version (required)

  NOTE: support for repo names is not within the scope of this projec and is a exercise
        left to the user
EOM
  exit 1
}

while getopts ":b:Cc:dhi:r:Ttv:" opt
do
  case "$opt" in
    b ) BUILD_NUMBER=${BUILD_NUMBER} ;;
    C ) ((CLEANUP+=1)) ;;
    c ) CODENAMES=${OPTARG} ;;
    d )
      ((DEBUG+=1))
      set -x
    ;;
    h )
      usage
    ;;
    i ) ITERATION=${OPTARG} ;;
    r ) REPONAME=${OPTARG} ;;
    T ) BUILD_DEBUG=1 ;;
    t ) TEST_DEBUG=1 ;;
    v ) VERSION=${OPTARG} ;;
    * )
      echo "Unexpected option \"$opt\""
      usage
    ;;
  esac
done
shift $((OPTIND - 1))

[[ -z "$CODENAMES" ]] && CODENAMES=all

ETC_DIR="$(readlink -f "${SCRDIR}/../etc")"

for vf in global.vars .secret.vars
do
  SCRATCH="${ETC_DIR}/${vf}"
  if [[ -r "$SCRATCH" ]]
  then
    echo "sourcing $SCRATCH"
    source "$SCRATCH"
  fi
done

PROJECT_DIR="$PWD"
WORK_DIR="${PROJECT_DIR}/work"
PROJECT_VARS="${PROJECT_DIR}/project.vars"

if [[ ! -r "$PROJECT_VARS" ]]
then
  cat << EOF
    This script is designed to run from the top of your package build project
    and expects a file named project.vars to be there and readable.

    File not found: $PROJECT_VARS !!!
EOF
  exit 1
fi

echo "sourcing $PROJECT_VARS"
source "$PROJECT_VARS"

if [[ "$CLEANUP" != "0" ]]
then
  cleanup
fi

no_source_dir() {

  cat << EOF
    the SOURCE_DIR variable was not defined or the directory defined was not found

    SOURCE_DIR should be defined in your project directory and probably should
    be a sub directory of WORK_DIR
    preparing the source directory will take a while to automate for various projects
    until then I'll leave it a manual step.

    Here is how I would prepare the source for the chruby version 0.3.9 project:
      mkdir work
      cd work
      git clone https://github.com/postmodern/chruby.git
      cd chruby
      git checkout -b v0.3.9 v0.3.9
      cd ../..
EOF
  exit 1
}

([[ -z "$SOURCE_DIR" ]] || [[ ! -d "$SOURCE_DIR" ]]) && no_source_dir

if [[ -z "$PACKAGE_DIR" ]]
then
  PACKAGE_DIR="${WORK_DIR}/packages-built"
  mkdir -p "$PACKAGE_DIR"
fi

echo "Successfully built packages will be saved in: $PACKAGE_DIR"

# SRCD="./ruby-install-${VERSION}"
# if [[ ! -d "$SRCD" ]]
# then
#   TBAL="/tmp/ruby-install-${VERSION}.tar.gz"
#   curl -C - -L -o "$TBAL" "https://github.com/postmodern/ruby-install/archive/v${VERSION}.tar.gz"
#   tar -xzf "$TBAL"
#   if [[ ! -d "$SRC_DIR" ]]
#   then
#     echo "Directory not found: $SRCD" >&2
#     exit 1
#   fi
# fi

if [[ "$CODENAMES" == "all" ]]
then
  for df in $(ls packaging/targets/build-image/Dockerfile.* 2>/dev/null)
  do
    CN=$(echo "$df" | sed 's!.*Dockerfile\.!!')
    if [[ -n "$CNAMES" ]]
    then
      CNAMES="$CNAMES $CN"
    else
      CNAMES="$CN"
    fi
  done
else
  CNAMES="$CODENAMES"
fi

for cn in $CNAMES
do
  case "$cn" in
    buster )
      OSNAME=debian
    ;;
    focal|impish )
      OSNAME=ubuntu
    ;;
    * )
      echo "Unsupported codename: $cn" >&2
      exit 1
    ;;
  esac

  cat << EOF > ./packaging/env.list
PRODUCT_NAME=$PRODUCT_NAME
SOURCE_DIR=$SOURCE_DIR
VERSION=$VERSION
DCODENM=$cn
OSNAME=$OSNAME
BUILD_NUMBER=$ITERATION
BUILD_DEBUG=$BUILD_DEBUG
TEST_DEBUG=$TEST_DEBUG
EOF

  packaging/targets/build
  RC=$?
  if [[ "$RC" == "0" ]]
  then
    for deb in $(ls packaging/targets/out/*.deb 2>/dev/null)
    do
      mv -fv "$deb" "$PACKAGE_DIR"
    done
  else
    echo "packaging/targets/build exit code: $RC" >&2
    ((ERRORS+=1))
  fi
done


# ERRORS=$((ERRORS+=1)) # darn ubuntu default dash shell
exit $ERRORS

# vim:sta:et:sw=2:ts=2:syntax=sh
